output "vpc_arn" {
  value       = aws_vpc.lmarcia_melt_platform_ecs_workshop.arn
  description = "Expected output after creation"
}
