resource "aws_vpc" "lmarcia_melt_platform_ecs_workshop" {
  cidr_block       = "10.0.0.0/24"
  instance_tenancy = "default"

  tags = {
    Name = var.vpc_name
  }

}
