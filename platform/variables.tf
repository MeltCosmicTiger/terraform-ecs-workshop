variable "vpc_name" {
  type        = string
  description = "A simple name for the test on VPC of EC2 platform"
}
