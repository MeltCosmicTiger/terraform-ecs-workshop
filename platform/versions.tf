terraform {
  required_version = ">=1" # uses TF major than 1
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.22.1"
    }
  }
}
